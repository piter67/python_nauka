#stałe
KONIEC_PRZEDZIALU = 10000

# Funkcja przykładowa, najprostsza z możliwych :)
def dodajDwa(liczba):
    return liczba+2
# Funkcja która przyjmuje dwa argumenty
def czySieDzieli(a,b):
    if(a%b == 0):
        return "TAK"
    else:
        return "NIE"
# Funkcja sprawdza, czy liczba jest pierwsza
def czyLiczbaPierwsza(liczba):
    for cos in range(2,liczba):
        if(liczba%cos == 0):
            return 0
    return 1

# Poczatek programu głównego
try:
    zakres = int(input("Do jakiej wartości maksymalnej szukać liczb pierwszych? : "))
except: #Obsługa wyjątku konwersji podanej wartości na liczbę int
    print("To co podałeś,nie jest liczbą,kończę !")
    exit()

#Sprawdzenie, czy wprowadzona liczba jest w rozsądnym zakresie
if(zakres > KONIEC_PRZEDZIALU or zakres < 2):
    print("Podaj liczbę z przedziału 2 - "+str(KONIEC_PRZEDZIALU))
    exit()

# Obliczenie liczb pierwszych w zakresie
print("Poniżej wszystkie liczby pierwsze z zakresu 1 - "+str(zakres)+' :')
for a in range(3,zakres+1):
    if(czyLiczbaPierwsza(a)):
        print(str(a)+",", end='')

